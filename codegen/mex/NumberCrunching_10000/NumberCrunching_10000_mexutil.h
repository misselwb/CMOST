/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * NumberCrunching_10000_mexutil.h
 *
 * Code generation for function 'NumberCrunching_10000_mexutil'
 *
 */

#ifndef __NUMBERCRUNCHING_10000_MEXUTIL_H__
#define __NUMBERCRUNCHING_10000_MEXUTIL_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_10000_types.h"

/* Function Declarations */
extern const mxArray *b_emlrt_marshallOut(const real_T u);

#endif

/* End of code generation (NumberCrunching_10000_mexutil.h) */
