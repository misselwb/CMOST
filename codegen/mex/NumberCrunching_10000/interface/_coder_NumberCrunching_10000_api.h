/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_NumberCrunching_10000_api.h
 *
 * Code generation for function '_coder_NumberCrunching_10000_api'
 *
 */

#ifndef ___CODER_NUMBERCRUNCHING_10000_API_H__
#define ___CODER_NUMBERCRUNCHING_10000_API_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_10000_types.h"

/* Function Declarations */
extern void NumberCrunching_10000_api(NumberCrunching_10000StackData *SD, const
  mxArray *prhs[26], const mxArray *plhs[29]);

#endif

/* End of code generation (_coder_NumberCrunching_10000_api.h) */
