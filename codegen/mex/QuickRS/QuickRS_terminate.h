/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * QuickRS_terminate.h
 *
 * Code generation for function 'QuickRS_terminate'
 *
 */

#ifndef __QUICKRS_TERMINATE_H__
#define __QUICKRS_TERMINATE_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "QuickRS_types.h"

/* Function Declarations */
extern void QuickRS_atexit(void);
extern void QuickRS_terminate(void);

#endif

/* End of code generation (QuickRS_terminate.h) */
