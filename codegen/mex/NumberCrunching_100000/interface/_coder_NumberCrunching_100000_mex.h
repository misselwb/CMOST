/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_NumberCrunching_100000_mex.h
 *
 * Code generation for function '_coder_NumberCrunching_100000_mex'
 *
 */

#ifndef ___CODER_NUMBERCRUNCHING_100000_MEX_H__
#define ___CODER_NUMBERCRUNCHING_100000_MEX_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_100000_types.h"

/* Function Declarations */
extern void mexFunction(int32_T nlhs, mxArray *plhs[], int32_T nrhs, const
  mxArray *prhs[]);
extern emlrtCTX mexFunctionCreateRootTLS(void);

#endif

/* End of code generation (_coder_NumberCrunching_100000_mex.h) */
