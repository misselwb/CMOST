/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_NumberCrunching_50000_api.h
 *
 * Code generation for function '_coder_NumberCrunching_50000_api'
 *
 */

#ifndef ___CODER_NUMBERCRUNCHING_50000_API_H__
#define ___CODER_NUMBERCRUNCHING_50000_API_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_50000_types.h"

/* Function Declarations */
extern void NumberCrunching_50000_api(NumberCrunching_50000StackData *SD, const
  mxArray *prhs[26], const mxArray *plhs[29]);

#endif

/* End of code generation (_coder_NumberCrunching_50000_api.h) */
