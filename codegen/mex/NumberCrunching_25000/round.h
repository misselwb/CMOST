/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * round.h
 *
 * Code generation for function 'round'
 *
 */

#ifndef __ROUND_H__
#define __ROUND_H__

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "NumberCrunching_25000_types.h"

/* Function Declarations */
extern void b_round(real_T x[25]);

#endif

/* End of code generation (round.h) */
