## CMOST (Colon Modeling Open Source Tool)
### Please see file "CMOST_Manual.pdf" for use instructions.


M.K. Prakash, B. Lang, H. Heinrich, P.V. Valli, P. Bauerfeind, A. Sonnenberg, et al., CMOST: an open-source framework for the microsimulation of colorectal cancer screening strategies, BMC Med Inform Decis Mak. 17 (2017) 225.

https://doi.org/10.1186/s12911-017-0458-9